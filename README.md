# Trabajo Integrador del Semillero
Integrantes:
- Bertó
- Bristot
- Obredor
- Suarez

## Consignas
https://docs.google.com/document/d/1FjKxSDXP0YTtJnMQUm464h8JzZgE7f_adMXRrEO4Qhw/edit#
## Presentación
https://docs.google.com/presentation/d/1XxNOERk-3XmjmsV2DowwqYLM-beTpxYzxBjIxn7_vEw/edit?usp=sharing

# Implementación

## Arquitectura
para ver diagramas más actualizados, carpeta doc

### Orquestación del módulo de Procesamiento y Presentación a traves de un Main

```plantuml
    Main --> Procesamiento: Procesamiento()
    Procesamiento --> CsvHandler: CsvHandler()

    CsvHandler -> CsvHandler: open(archivo.csv)
    CsvHandler -> CsvHandler: read_csv(archivo.csv)

    Main -> Procesamiento: get_nro_nodos()
    Main <- Procesamiento: nroNodos

    Main --> Presentacion: Presentacion(nroNodos)
    Presentacion -> Pyplot: figure()
    Presentacion -> Pyplot: figure()
    Presentacion -> Pyplot: figure()



    Main -> Procesamiento: temps_todos_nodos()
    Main <- Procesamiento: datos

    Main -> Presentacion: set_datos(datos)


    Main -> Presentacion: poblarGraficos()

    Presentacion -> Presentacion: box_plot()
    Presentacion -> Presentacion: evolucion_cada_nodo()
    Presentacion -> Presentacion: evolucion_promedio_todos()


    Main -> Presentacion: mostrarGraficos()
    Presentacion -> Pyplot: show()

```

## Entorno de desarrollo y ejecución

### Software embebido

#### Requerimientos de software

1. Eclipse C/C++
1. Espressif Tolchain
1. cmake version 5.1 o superior
1. Python 3.6 o superior

#### Compilación y ejecución

1. Crear nuevo proyecto de espressif a partir de toolchain.
1. Copiar `app_main.c`.
1. Compilar y grabar.

### Procesamiento y análisis de datos

```
conda env create -f environment.lock.yaml
conda activate semillero
cd src
python Main.py
```
