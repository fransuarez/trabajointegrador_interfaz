/* MQTT (over TCP) Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
 */

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_attr.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"
#include "sntp.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include <stddef.h>
#include "esp_wifi.h"
#include "esp_netif.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"

#include "esp_freertos_hooks.h"


#include "driver/adc.h"
#include "esp_adc_cal.h"

#define EJECUCIONES_TICK	200
#define EJECUCIONES_IDLE	500
#define DEFAULT_VREF    1100        //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES   64          //Multisampling
#define ID		17
#define TOPIC	"sensores/nodo_17"

static const char *TAG = "MQTT_EXAMPLE";
static const char *TAG_TIME = "TIME";
static esp_adc_cal_characteristics_t *adc_chars;
#if CONFIG_IDF_TARGET_ESP32
static const adc_channel_t channel = ADC_CHANNEL_6;     //GPIO34 if ADC1, GPIO14 if ADC2
static const adc_bits_width_t width = ADC_WIDTH_BIT_12;
#elif CONFIG_IDF_TARGET_ESP32S2
static const adc_channel_t channel = ADC_CHANNEL_6;     // GPIO7 if ADC1, GPIO17 if ADC2
static const adc_bits_width_t width = ADC_WIDTH_BIT_13;
#endif
static const adc_atten_t atten = ADC_ATTEN_DB_0;
static const adc_unit_t unit = ADC_UNIT_1;

SemaphoreHandle_t semaforo;
QueueHandle_t xQueueAdc;
esp_mqtt_client_handle_t client;

static void check_efuse(void)
{
#if CONFIG_IDF_TARGET_ESP32
	//Check if TP is burned into eFuse
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
		printf("eFuse Two Point: Supported\n");
	} else {
		printf("eFuse Two Point: NOT supported\n");
	}
	//Check Vref is burned into eFuse
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
		printf("eFuse Vref: Supported\n");
	} else {
		printf("eFuse Vref: NOT supported\n");
	}
#elif CONFIG_IDF_TARGET_ESP32S2
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
		printf("eFuse Two Point: Supported\n");
	} else {
		printf("Cannot retrieve eFuse Two Point calibration values. Default calibration values will be used.\n");
	}
#else
#error "This example is configured for ESP32/ESP32S2."
#endif
}


static void print_char_val_type(esp_adc_cal_value_t val_type)
{
	if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
		printf("Characterized using Two Point Value\n");
	} else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
		printf("Characterized using eFuse Vref\n");
	} else {
		printf("Characterized using Default Vref\n");
	}
}

/*Esta tarea se usa para iniciallizar el
 * sntp*/
static void init_sntp_task(void *arg)
{
	int retry = 0;
	const int retry_count = 10;

	sntp_servermode_dhcp(1);

	ESP_LOGI(TAG_TIME, "Initializing SNTP");
	sntp_setoperatingmode(SNTP_OPMODE_POLL);
	sntp_setservername(0, "pool.ntp.org");
	sntp_init();

	while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < retry_count)
	{
		ESP_LOGI(TAG_TIME, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
		vTaskDelay(pdMS_TO_TICKS(2000));
	}
	vTaskDelete(NULL);
}

/*Esta tarea se desbloquea cada vez que hay un valor de ADC
 * en la xQueueAdc, obtiene el timeStap por sntp y arma el Json
 * para ebviar por MQTT*/
void com_task(void *arg)
{
	char strftime_buf[40];
	char msgPayload[100];
	time_t now;
	struct tm timeinfo;
	struct timeval outdelta;
	float temp;

	vTaskDelay(pdMS_TO_TICKS(4000));    //Wait a few secs to be sure the WiFi is ok

	//Get time, then config and set time zone
	time(&now);
	localtime_r(&now, &timeinfo);
	setenv("TZ", "WART4WARST,J1/0,J365/25", 1);
	tzset();
	if (sntp_get_sync_mode() == SNTP_SYNC_MODE_SMOOTH)
	{
		while (sntp_get_sync_status() == SNTP_SYNC_STATUS_IN_PROGRESS)
		{
			adjtime(NULL, &outdelta);
			ESP_LOGI(TAG_TIME, "Waiting for adjusting time ... outdelta = %li sec: %li ms: %li us",
					(long)outdelta.tv_sec,
					outdelta.tv_usec/1000,
					outdelta.tv_usec%1000);
			vTaskDelay(pdMS_TO_TICKS(2000));
		}
	}

	while(1)
	{
		if (xQueueReceive(xQueueAdc,&temp, portMAX_DELAY)){
			time(&now);
			localtime_r(&now, &timeinfo);
			strftime(strftime_buf, sizeof(strftime_buf), "%d/%m/%Y %H:%M:%S", &timeinfo);
			ESP_LOGI(TAG_TIME, "%s", strftime_buf);
			sprintf (msgPayload, "{\"id\":%d, \"temp\": %.2f, \"timestap\": %s}",ID,temp/10,strftime_buf);
			esp_mqtt_client_publish(client, TOPIC, msgPayload,0,0,0);
		}
	}
	vTaskDelete(NULL);
}

static void log_error_if_nonzero(const char *message, int error_code)
{
	if (error_code != 0) {
		ESP_LOGE(TAG, "Last error %s: 0x%x", message, error_code);
	}
}

/*
 * @brief Event handler registered to receive MQTT events
 *
 *  This function is called by the MQTT client event loop.
 *
 * @param handler_args user data registered to the event.
 * @param base Event base for the handler(always MQTT Base in this example).
 * @param event_id The id for the received event.
 * @param event_data The data for the event, esp_mqtt_event_handle_t.
 */
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
	ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
	esp_mqtt_event_handle_t event = event_data;
	esp_mqtt_client_handle_t client = event->client;
	int msg_id;
	switch ((esp_mqtt_event_id_t)event_id) {
	case MQTT_EVENT_CONNECTED:
		ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
		msg_id = esp_mqtt_client_publish(client, "/topic/qos1", "data_3", 0, 1, 0);
		ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);

		msg_id = esp_mqtt_client_subscribe(client, "/topic/qos0", 0);
		ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

		msg_id = esp_mqtt_client_subscribe(client, "/topic/qos1", 1);
		ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

		msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
		ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
		break;
	case MQTT_EVENT_DISCONNECTED:
		ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
		break;

	case MQTT_EVENT_SUBSCRIBED:
		ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
		msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
		ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
		break;
	case MQTT_EVENT_UNSUBSCRIBED:
		ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
		break;
	case MQTT_EVENT_PUBLISHED:
		ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
		break;
	case MQTT_EVENT_DATA:
		ESP_LOGI(TAG, "MQTT_EVENT_DATA");
		printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
		printf("DATA=%.*s\r\n", event->data_len, event->data);
		break;
	case MQTT_EVENT_ERROR:
		ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
		if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
			log_error_if_nonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
			log_error_if_nonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
			log_error_if_nonzero("captured as transport's socket errno",  event->error_handle->esp_transport_sock_errno);
			ESP_LOGI(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));

		}
		break;
	default:
		ESP_LOGI(TAG, "Other event id:%d", event->event_id);
		break;
	}
}

static void mqtt_app_start(void)
{
	esp_mqtt_client_config_t mqtt_cfg = {
			.uri = CONFIG_BROKER_URL,
	};
#if CONFIG_BROKER_URL_FROM_STDIN
	char line[128];

	if (strcmp(mqtt_cfg.uri, "FROM_STDIN") == 0) {
		int count = 0;
		printf("Please enter url of mqtt broker\n");
		while (count < 128) {
			int c = fgetc(stdin);
			if (c == '\n') {
				line[count] = '\0';
				break;
			} else if (c > 0 && c < 127) {
				line[count] = c;
				++count;
			}
			vTaskDelay(10 / portTICK_PERIOD_MS);
		}
		mqtt_cfg.uri = line;
		printf("Broker url: %s\n", line);
	} else {
		ESP_LOGE(TAG, "Configuration mismatch: wrong broker url");
		abort();
	}
#endif /* CONFIG_BROKER_URL_FROM_STDIN */

	client = esp_mqtt_client_init(&mqtt_cfg);
	/* The last argument may be used to pass data to the event handler, in this example mqtt_event_handler */
	esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
	esp_mqtt_client_start(client);

}

/*Esta tarea se ejecuta mediante un semaforo  el cual la function tickHook le hace give
 * cada cierta cantidad de ticks del sistema, toma valor del ADC y lo coloca en una queue*/
void medAdc_task(void *arg)
{
	uint32_t adc_reading = 0;

	while(1){
		xSemaphoreTake(semaforo,portMAX_DELAY);
		for (int i = 0; i < NO_OF_SAMPLES; i++) {
			adc_reading += adc1_get_raw((adc1_channel_t)channel);
		}
		adc_reading /= NO_OF_SAMPLES;
		float voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
		xQueueSend(xQueueAdc,&voltage,portMAX_DELAY);

	}
}

/**
 * Esta funcion se ejecuta cada vez que se produce un tick del sistema
 */
void myTickHook( void )  {
	static uint32_t ejecuciones_tick = 0;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	/**
	 * Este sencillo codigo lo que hace es cada 25 ticks del sistema
	 * liberar el semaforo y decirle al sistema operativo si debe
	 * o no hacer un rescheduling, para eso sirve la variable xHigherPriorityTaskWoken
	 */
	ejecuciones_tick++;
	if(ejecuciones_tick % EJECUCIONES_TICK == 0)  {
		xSemaphoreGiveFromISR(semaforo,&xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
}

void app_main(void)
{
	BaseType_t xReturned;

	ESP_LOGI(TAG, "[APP] Startup..");
	ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
	ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

	ESP_ERROR_CHECK(nvs_flash_init());
	ESP_ERROR_CHECK(esp_netif_init());
	ESP_ERROR_CHECK(esp_event_loop_create_default());

	check_efuse();
	adc1_config_width(width);
	adc1_config_channel_atten(channel, atten);

	//Characterize ADC
	adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, width, DEFAULT_VREF, adc_chars);
	print_char_val_type(val_type);


	/* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
	 * Read "Establishing Wi-Fi or Ethernet Connection" section in
	 * examples/protocols/README.md for more information about this function.
	 */
	semaforo = xSemaphoreCreateBinary();
	xQueueAdc = xQueueCreate( 10, sizeof( float ) );

	if (xQueueAdc == NULL){
		ESP_LOGE(TAG,"Error de creacion de queue por falta de memoria\n");
		while(1);
	}
	if (semaforo == NULL)  {
		ESP_LOGE(TAG,"Error de creacion de semaforo por falta de memoria\n");
		while(1);
	}

	if(esp_register_freertos_tick_hook(myTickHook) != ESP_OK)  {
		ESP_LOGE(TAG,"Error de registro de tick hook por falta de memoria\n");
		while(1);
	}

	ESP_ERROR_CHECK(example_connect());

	xReturned =  xTaskCreate(init_sntp_task, "init_sntp_task", 4096, NULL, 4, NULL);

	if(xReturned == pdPASS)  {
		ESP_LOGI(TAG,"Creacion de tarea con exito\n");
	}
	else {
		ESP_LOGE(TAG,"Error de creacion con codigo de error %d\n",xReturned);

		//Todavia no tenemos medio de captar un error, asi que capturamos el programa aca
		while(1);
	}

	mqtt_app_start();

	xReturned = xTaskCreate(com_task, "com_task", 4096 ,NULL, 3, NULL);

	if(xReturned == pdPASS)  {
		ESP_LOGI(TAG,"Creacion de tarea con exito\n");
	}
	else {
		ESP_LOGE(TAG,"Error de creacion con codigo de error %d\n",xReturned);

		//Todavia no tenemos medio de captar un error, asi que capturamos el programa aca
		while(1);
	}

	xReturned = xTaskCreate(medAdc_task, "medAdc_task", 2048, NULL, 2, NULL);

	if(xReturned == pdPASS)  {
		ESP_LOGI(TAG,"Creacion de tarea con exito\n");
	}
	else {
		ESP_LOGE(TAG,"Error de creacion con codigo de error %d\n",xReturned);

		//Todavia no tenemos medio de captar un error, asi que capturamos el programa aca
		while(1);
	}
}
