import numpy as np
import matplotlib.pyplot as plt
from operator import add
from matplotlib.widgets import Button


class Presentacion:
    def __init__(self, nroNodos: int = 10):

        self.BOX_PLOT = 0  # TODO version barata de un enum
        self.EVOLUCION_CADA_NODO = 1
        self.EVOLUCION_PROMEDIO_TODOS = 2

        # referencia a figuras
        self.figBoxPlot = plt.figure(self.BOX_PLOT)
        self.figEvolCadaNodo = plt.figure(self.EVOLUCION_CADA_NODO)
        self.figEvolPromTodos = plt.figure(self.EVOLUCION_PROMEDIO_TODOS)

        self.datos = None
        self.nroNodos = nroNodos

        # botonera para seleccionar graficas
        self.indiceNodo = 0

    def next(self, event):
        if self.indiceNodo < self.nroNodos-1:
            self.indiceNodo += 1
            #print("indice : ", self.indiceNodo)
            self.actualizar_graf_evol_cada_nodo()

    def prev(self, event):
        if self.indiceNodo > 0:
            self.indiceNodo -= 1
            #print("indice : ", self.indiceNodo)
            self.actualizar_graf_evol_cada_nodo()

    def set_datos(self, dat):
        """recibe una lista de listas. cada lista tiene las mediciones de temperatura de un sensor"""
        self.datos = dat

    def poblarGraficos(self):
        if self.datos is not None:
            self.box_plot()
            self.evolucion_cada_nodo()
            self.evolucion_promedio_todos()
        else:
            raise ValueError('Error: no hay datos para presentar!!!')

    def mostrarGraficos(self):
        plt.show()

    def actualizarGraficos(self):
        plt.draw()

    def getNroNodos(self):
        return self.nroNodos

    def evolucion_cada_nodo(self):
        """evolución de la temperatura de cada nodo"""
        fig = self.figEvolCadaNodo
        plt.figure(fig.number)  # setear figura activa
        fig.canvas.manager.set_window_title(
            'Evolución de la temperatura de cada Nodo')
        # botonera para seleccionar graficas
        self.axprev = plt.axes([0.7, 0.05, 0.1, 0.075])
        self.axnext = plt.axes([0.81, 0.05, 0.1, 0.075])
        self.bnext = Button(self.axnext, 'Siguiente')
        self.bnext.on_clicked(self.next)
        self.bprev = Button(self.axprev, 'Anterior')
        self.bprev.on_clicked(self.prev)
        # agregar plot para ejes cartesianos
        fig.add_subplot()

        self.actualizar_graf_evol_cada_nodo()

    def actualizar_graf_evol_cada_nodo(self):
        """volver a graficar ejes cuando se aprieta algun boton"""
        fig = self.figEvolCadaNodo
        plt.figure(fig.number)
        # limpiar ejes antes de volver a dibujar
        dondeGraficar = 2
        ejes = fig.get_axes()
        #print('ejes : ', ejes)
        ejes[dondeGraficar].clear()
        ejes[dondeGraficar].plot(self.datos[self.indiceNodo])
        ejes[dondeGraficar].set_title(
            'Evolución del Nodo : ' + str(self.indiceNodo+1))
        self.actualizarGraficos()

    def evolucion_promedio_todos(self):
        """evolución del promedio de temperatura de todos los nodos"""
        fig = self.figEvolPromTodos
        plt.figure(fig.number)
        fig.canvas.manager.set_window_title(
            'Evolución del promedio de temperatura de todos los nodos')
        # Subplots are organized in a Rows x Cols Grid
        Tot = self.getNroNodos()
        Cols = np.floor(np.sqrt(Tot))
        # Compute Rows required
        Rows = Tot // Cols
        Rows += Tot % Cols
        # Create a Position index
        Position = range(1, Tot + 1)
        # Create figure, add every single subplot to the figure with a for loop
        for k in range(Tot):
            ax = fig.add_subplot(Rows, Cols, Position[k])
            # TODO hasta ahora solo grafica la evolucion de la temperatura, es necesario usar datos de las medias
            ax.plot(self.datos[k])

    def box_plot(self):
        """box-plots de cada nodo"""
        fig = self.figBoxPlot
        plt.figure(fig.number)

        fig, ax = plt.subplots()
        fig.canvas.manager.set_window_title(
            'Box-plots(caja y bigotes) de cada nodo')
        ax.set_title('temperatura de cada nodo')
        ax.boxplot(self.datos)


if __name__ == "__main__":
    nroNod = 10
    tamanio = 15
    # Fixing random state for reproducibility
    np.random.seed(19680801)
    # fake up some data
    center = np.ones(tamanio) * 25
    # print('center', center)
    datos = []
    for k in range(nroNod):
        ruido = np.random.rand(tamanio)
        temperatura = list(map(add, center, ruido))
        #print('temperatura', temperatura)
        datos.append(temperatura)
    #print('datos', datos)

    p = Presentacion(nroNodos=nroNod)
    p.set_datos(datos)

    p.poblarGraficos()

    p.mostrarGraficos()
