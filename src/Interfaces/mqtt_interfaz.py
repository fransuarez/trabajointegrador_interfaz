import paho.mqtt.client as mqtt
import time


class MyMQTTClass(mqtt.Client):
    """
    Clase dedicada al manejo conexion con el broker mqtt

    :param n_clusters: Number of clusters used to segment the data.
    :type n_clusters: int
    """
    messages_list = []
    index_list = 0

    def on_connect(self, mqttc, obj, flags, rc):
        print("rc: "+str(rc))

    def on_message(self, mqttc, obj, msg):
        """
        Metodo que captura los mensajes recibidos desde el broker mqtt.

        :param mqttc: Referencia al objecto instanciado.
        :type mqttc: MyMQTTClass
        :param msg: Copia del mensaje obtenido.
        :type msg: String

        """
        print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
        # print("message received ", str(msg.payload.decode("utf-8")))
        self.messages_list.append(str(msg.payload.decode("utf-8")))
        self.index_list += 1

    def on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))

    def on_log(self, mqttc, obj, level, string):
        # print(string)
        pass

    def get_msg(self):
        """
        Retorna los mensajes guardados en cola.

        :return: El primer mensaje que esta en la cola de recepcion.
        :rtype: String con mensaje o "void" si esta vacio.
        """
        if self.messages_list.__len__() != 0:
            self.index_list -= 1
            return self.messages_list.pop(0)
        else:
            return "void"

    def run(self):
        # self.connect("mqtt.eclipseprojects.io", 1883, 60)
        # self.subscribe("$SYS/#", 0)
        rc = 0
        while rc == 0:
            rc = self.loop()
        return rc


class ClientDriver:
    """
    Clase dedicada al manejo de datos recibidos desde un broker.

    :param numero_nodos: Numero de nodos esperados en la red. No mandatorio.
    :type client_name: Nombre para identificar al cliente mqtt generado.
    """
    brokerAddress = "181.47.10.131"
    brokerPort = 1883
    brokerTopic = ""  # "test/topic"

    def __init__(self, numero_nodos: int, client_name: str):
        self.nroNodos = numero_nodos
        self.client = MyMQTTClass(client_id=client_name)

    def connection(self, topic: str):
        """
        Metodo que se encarga de generar la conexión al broker mqtt.

        :param topic: Topico al cual desea conectarse
        :type topic: String
        """
        self.brokerTopic = topic
        self.client.connect(host=self.brokerAddress, port=self.brokerPort)  # connect to broker
        print("Subscribing to topic!") #print("Subscribing to topic", self.brokerTopic)
        self.client.subscribe(topic=self.brokerTopic)

    def close(self):
        self.client.disconnect()

    def take_message(self):
        """
        Metodo que toma los mensajes guardados en cola por el cliente mqtt.

        :return: Un mensaje si encuentra o void.
        :rtype: String con mensaje o "void".
        """
        msg: str
        '''
        rc = self.client.run()
        if rc == 1:
            print("Mensaje recibido")
        '''
        self.client.loop_start()  # start the loop
        time.sleep(1)  # wait
        self.client.loop_stop()  # stop the loop
        msg = self.client.get_msg()
        # if msg == "void":
            # print("Sin Mensajes", msg)
        return msg

    def send_message(self, message: str):
        print("Publishing message to topic", self.brokerTopic)
        self.client.publish(topic=self.brokerTopic, payload=message)


if __name__ == "__main__":
    receiveMsg: str
    print('probando cliente mqtt')
    myDriver = ClientDriver(8, "my driver mqtt")
    myDriver.connection("test/topic")
    try:
        receiveMsg = myDriver.take_message()
    except KeyboardInterrupt:
        myDriver.close()

