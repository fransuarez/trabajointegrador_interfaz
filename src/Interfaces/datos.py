import json
import csv
import operator
import pathlib
from Interfaces.mqtt_interfaz import ClientDriver


class Infraestructura:
    def __init__(self, numero:int):
        self.NroNodos = numero

    def set_num_nodos(self, numero: int):
        self.NroNodos = numero

    def get_nro_nodos(self):
        return self.NroNodos


class ParserMqtt:
    cliente = ClientDriver(8, "my driver mqtt")

    def __init__(self, csv_path, topic_path):
        self.csv_path = csv_path
        self.write_header = not pathlib.Path(self.csv_path).exists()
        self.cliente.connection(topic_path)

    def json_decoding(self):
        # { “Id”: 4322, “temp”: 18°C, “timestamp”: "17:45, 12/02/2021" }"
        try:
            message = self.cliente.take_message()
            if message != "void":
                try:
                    datos_list = json.loads(message)
                    if self.write_header:
                        self.writerCsv.writerow(sorted(datos_list.keys()))
                        self.write_header = False
                    self.writerCsv.writerow(
                        value for _, value in sorted(datos_list.items(), key=operator.itemgetter(0))
                    )
                except StopIteration:
                    return "CONTINUE"
                except ValueError:
                    return "CONTINUE"
                return "OK"

        except KeyboardInterrupt:
            return "FIN"

    def close_file(self):
        self.csvFile.close()
        self.cliente.close()
        print("archivo csv cerrado correctamente")

    def open_file(self):
        self.csvFile = open(self.csv_path, "a")
        print("se abre correctamente??? self.csvFile:", self.csvFile)
        self.writerCsv = csv.writer(self.csvFile)


if __name__ == "__main__":
    print('Probando parser....')
    parser = ParserMqtt("test.csv", "sensores/#")

    parser.open_file()
    while parser.json_decoding() != "FIN":
        pass
    parser.close_file()

