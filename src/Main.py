from Presentacion.Presentacion import Presentacion
from Procesamiento.Procesamiento import Procesamiento
from Interfaces.datos import ParserMqtt


if __name__ == "__main__":

    rutaParaProcesamiento = 'test.csv'
    parser = ParserMqtt(rutaParaProcesamiento, "sensores/#")
    pro = Procesamiento(rutaParaProcesamiento)

    # count = 0
    nroConsultasServidor = 5
    #while (count < 3): # TODO actualizacion en tiempo real de las graficas
    # count = count + 1 #TODO solo en la primera iteracion descarga datos, posible problema de conexion o de archivos

    parser.open_file()
    iter = 0
    ret = 0
    while (iter < nroConsultasServidor and ret != "FIN"):
        iter = iter + 1
        print("iter = ", iter)
        ret = parser.json_decoding() # TODO posible problema cuando devuelve "FIN"
        if ret == "FIN":
            break
    parser.close_file()

    nroNodos = pro.get_nro_nodos()
    pre = Presentacion(nroNodos)
    dato = pro.temps_todos_nodos()
    # print("dato:")
    # print(dato)
    pre.set_datos(dato)
    pre.poblarGraficos()
    pre.mostrarGraficos()

