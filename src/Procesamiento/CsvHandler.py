import pandas as pd


class CsvHandler():
    def __init__(self, rutaArchivo):
        self.reader = []
        self.index = []
        self.ruta = rutaArchivo
        with open(self.ruta, newline='') as File:
            print("File = ", File)
            self.reader = pd.read_csv(File) # abre y cierra el archivo. si ya esta abierto, lo deja abierto
            self.index = self.reader['Id'].unique().tolist()
