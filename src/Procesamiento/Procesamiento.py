import numpy as np
from Procesamiento.CsvHandler import CsvHandler

class Procesamiento():

    def __init__(self,rutaArchivo):
        self.csv = CsvHandler(rutaArchivo)

    def get_nro_nodos(self):
        # aux = len(self.csv.index)
        # print("aux", type(aux))
        # print("self.csv.index", aux)
        return len(self.csv.index)

    def lista_maxs_temps(self):
        """devuelve una lista con el maximo de cada nodo. ordenado segun el ID, o sea de menor a mayor"""
        maxTemp = []
        'For para procesar para cada nodo'
        for n in self.csv.index:
            dataTemp = self.csv.reader.loc[self.csv.reader['Id'] == n]
            maxTemp.append(dataTemp["temp"].max())
        'print (maxTemp)'
        return maxTemp

    def lista_mins_temps(self):
        """devuelve una lista con el minimo de cada nodo. ordenado segun el ID, o sea de menor a mayor"""
        minTemp = []
        'For para procesar para cada nodo'
        for n in self.csv.index:
            dataTemp = self.csv.reader.loc[self.csv.reader['Id'] == n]
            minTemp.append(dataTemp["temp"].min())
        'print (mintemp)'
        return minTemp

    def lista_medias_temps(self):
        """devuelve una lista con la media de cada nodo. ordenado segun el ID, o sea de menor a mayor"""
        meanTemp = []
        for n in self.csv.index:
            dataTemp = self.csv.reader.loc[self.csv.reader['Id'] == n]
            meanTemp.append(dataTemp["temp"].mean())
        'print (meanTemp)'
        return meanTemp

    def lista_temps_un_nodo(self,nroNodo):
        """devuelve una lista con las mediciones de temperatura de un sensor"""
        nodoTemp = []
        nodoTemp = self.csv.reader.loc[self.csv.reader['Id'] == nroNodo]
        print("nodoTemps, nroNodo seleccionado : ")
        print(nodoTemp)
        last_column = nodoTemp.iloc[:,1].tolist() #lista de mediciones #TODO acceder a las temperaturas usando el indice "temp" en vez del 1
        #print("last_column", last_column)
        return last_column

    def temps_todos_nodos(self):
        """devuelve una lista de listas. cada lista tiene todas las mediciones de un sensor"""
        dato = []
        print("range", range(1,self.get_nro_nodos()+1))
        for k in range(1,self.get_nro_nodos()+1):# TODO debe iniciar en 1 porque no existe el nodo "cero"
            dato.append(list(self.lista_temps_un_nodo(k)))
        # print("las mediciones de todos los nodos son: ")
        # print(dato)
        return dato

    def lista_desv_temps(self):
        """devuelve una lista con la desviacion estandar de cada nodo. ordenado segun el ID, o sea de menor a mayor"""
        desvTemp = []
        for n in self.csv.index:
            dataTemp = self.csv.reader.loc[self.csv.reader['Id'] == n]
            desvTemp.append(np.std(dataTemp["temp"]))
        print(desvTemp)


    """funciones para obtener estadisticas de todos los nodos"""

    def maxTemp(self):
        maxTempId = []
        '''maximo de todas las temperaturas'''
        # TODO me falta saber cual es el nodo de esa temperatura
        maxTemp = max(self.lista_maxs_temps())
        indexMax = (self.lista_maxs_temps()).index(maxTemp)
        idMax = self.csv.index [indexMax]
        print(maxTemp)
        maxTempId.append(idMax)
        maxTempId.append(maxTemp)
        print(maxTempId)
        return maxTempId     

    def minTemp(self):
        minTempId = []
        '''minimo de todas las temperaturas'''
        minTemp = min(self.lista_mins_temps())
        indexMin = (self.lista_mins_temps()).index(minTemp)
        idMin = self.csv.index [indexMin]
        print(minTemp)
        minTempId.append(idMin)
        minTempId.append(minTemp)
        print(minTempId)
        return minTempId

    def mediaTemp(self):
        meanTempId = []
        '''media de todas las mediciones de todos los nodos'''
        meanTemp = self.csv.reader["temp"].mean()
        print(meanTemp)
        return meanTemp

    def desvTemps(self):
        '''desviacion de todas las mediciones de todos los nodos'''
        desvTemp = self.csv.reader["temp"].std()
        print(desvTemp)


if __name__ == "__main__":
    # codigo para probar y debugear esta clase
    pro = Procesamiento()
    nroNodo = 10
    pro.lista_temps_un_nodo(nroNodo)
    pro.get_nro_nodos()
    pro.maxTemp()
    pro.minTemp()
